//let maxPoints=20
//let studentScore=15


let gradeCalc=function(maxPoints, studentScore){
    if(!(typeof maxPoints==='number'&&typeof studentScore==='number')){
        throw Error('Provide numbers');
    }
    let percentPoints=(studentScore / maxPoints)*100
        if(percentPoints>=90){
            return `You got a A (${percentPoints}%)!`
        }else if(percentPoints>=80){
            return `You got a B (${percentPoints}%)!`
        }else if(percentPoints>=70){
            return `You got a C (${percentPoints}%)!`
        }else if(percentPoints>=60){
            return `You got a D (${percentPoints}%)!`
        }else{
            return `You got a F (${percentPoints}%)!`
        }
}

try{
    let result=gradeCalc('hej', 15)
    console.log(result)
}catch(e){
    console.log(e.message)
}
