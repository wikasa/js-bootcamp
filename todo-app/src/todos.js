import { v4 as uuidv4 } from 'uuid';

let todos = [];

const loadTodos = () => {
    const todosJSON = localStorage.getItem('todos');
    try{
        return todosJSON ? JSON.parse(todosJSON) : [];
    }catch(e){
        return [];
    }
};

const saveTodos = () => {
    localStorage.setItem('todos', JSON.stringify(todos));
};

const getTodos = () => todos;

const createTodo = (text) => {
    if(text.length > 0){
        todos.push({
            completed:false,
            body: text,
            id: uuidv4()
        });
    saveTodos();
    }
};

const removeTodo = (id) => {
    const todoId = todos.findIndex((todo) => {
        return todo.id === id;
    });
    if(todoId > -1){ todos.splice(todoId, 1);}
};

const toggleTodo = (id) => {
    const todo = todos.find((todo) => {
        return todo.id === id;
    });
    if (todo){
        todo.completed = !todo.completed;
    }
};

todos = loadTodos();

export {loadTodos, saveTodos, createTodo, getTodos, removeTodo, toggleTodo};