const filters={
    searchTodo:'',
    hideCompleted:false
};

const getFilters = () => filters;

const setFilters = ({searchTodo, hideCompleted}) => {
    if(typeof searchTodo === 'string'){
        filters.searchTodo = searchTodo;
    }
    if(hideCompleted !== undefined){
        filters.hideCompleted = !filters.hideCompleted;
    }
};

export {getFilters, setFilters};