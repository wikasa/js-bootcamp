import {setFilters} from './filters';
import {loadTodos, createTodo} from './todos';
import {renderTodos} from './views';

renderTodos();

document.querySelector('#search-todo').addEventListener('input', (e) => {
    setFilters({
        searchTodo: e.target.value
    });
    renderTodos();
});

document.querySelector('#hide').addEventListener('change', (e) => {
    setFilters({
        hideCompleted: e.target.value
    });
    renderTodos();
})

document.querySelector('#adding-todo').addEventListener('submit', (e) => {
    e.preventDefault();
    const text = e.target.elements.todo.value.trim();
    createTodo(text);
    renderTodos();
    e.target.elements.todo.value='';
});

window.addEventListener('storage', (e) => {
    if(e.key === 'todos'){
        loadTodos();
        renderTodos();
    }
});