import {getFilters} from './filters';
import {getTodos, removeTodo, toggleTodo} from './todos';

const renderTodos = () => {
    const todos = getTodos();
    const {searchTodo, hideCompleted} = getFilters();

    const filteredTodos=todos.filter((todo) => {
        const searchTodoMatch=todo.body.toLowerCase().includes(searchTodo.toLowerCase());
        return searchTodoMatch && !(todo.completed && hideCompleted);
    })

    const notDone=filteredTodos.filter((todo) => {
        return !todo.completed
    })

    const todosEl = document.querySelector('#todos');
    todosEl.innerHTML='';
    todosEl.appendChild(generateSummaryDOM(notDone));
    if(filteredTodos.length > 0){
        filteredTodos.forEach((todo) => {
            todosEl.appendChild(generateTodoDOM(todo));
        });
    }
    else{
        const messageEl = document.createElement('p');
        messageEl .textContent = 'No to-dos to show';
        messageEl .classList.add('empty-message');
        todosEl.appendChild(messageEl);
    }
};

const generateTodoDOM = (todo) => {
    const todoEl = document.createElement('label');
    const containerEl = document.createElement('div');
    const checkbox = document.createElement('input');
    const todoText = document.createElement('span');
    const button = document.createElement('button');

    checkbox.setAttribute('type', 'checkbox');
    checkbox.checked = todo.completed;
    containerEl.appendChild(checkbox);
    checkbox.addEventListener('change', () => {
        toggleTodo(todo.id);
        renderTodos();
    })
    todoText.textContent=((todo.body.length > 0) ? todo.body : '');
    containerEl.appendChild(todoText);

    todoEl.classList.add('list-item');
    containerEl.classList.add('list-item__container');
    todoEl.appendChild(containerEl);

    button.textContent=('remove');
    button.classList.add('button', 'button--text')
    todoEl.appendChild(button);
    button.addEventListener('click', () => {
        removeTodo(todo.id);
        renderTodos();
    });
    return todoEl;
};

const generateSummaryDOM = (notDone) => {
    const summary=document.createElement('h2');
    const plural = notDone.length > 1 ? 's' : '';
    summary.textContent=`You have ${notDone.length} todo${plural} left`;
    summary.classList.add('list-title');
    return summary;
};

export {renderTodos, generateTodoDOM, generateSummaryDOM};