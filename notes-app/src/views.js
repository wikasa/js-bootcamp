import moment from 'moment';
import {getFilters} from './filters';
import {sortNotes, getNotes} from './notes';

const generateNoteDOM = (note) => {
    const noteEl = document.createElement('a');
    const textEl = document.createElement('p');
    const statusEl = document.createElement('p');

    textEl.textContent=((note.title.length > 0) ? note.title : 'Unnamed note');
    textEl.classList.add('list-item__title');
    noteEl.appendChild(textEl);

    noteEl.setAttribute('href', `/edit.html#${note.id}`);
    noteEl.classList.add('list-item');

    statusEl.textContent = generateLastEdited(notes.updatedAt);
    statusEl.classList.add('list-item__subtitle');
    noteEl.appendChild(statusEl);

    return noteEl;
};

const renderNotes = () => {
    const notesEl = document.querySelector('#notes');
    const filters = getFilters();
    const notes = sortNotes(filters.sortBy);
    const filteredNotes=notes.filter(note => {
        return note.title.toLowerCase().includes(filters.searchText.toLowerCase());
    });
    notesEl.innerHTML='';
    if(filteredNotes.length > 0){
        filteredNotes.forEach(note => {
            const noteEl = generateNoteDOM(note);
            notesEl.appendChild(noteEl);
        });
    }
    else{
        const emptyMessage = document.createElement('p');
        emptyMessage.textContent = 'No notes found';
        emptyMessage.classList.add('empty-message');
        notesEl.appendChild(emptyMessage);
    }
};

const initializeEditPage = (noteId) => {
    const titleEl = document.querySelector('#note-title');
    const dateEl = document.querySelector('#last-edited');
    const bodyEl  = document.querySelector('#note-body');
    const notes = getNotes();
    const note = notes.find((note) => {
        return note.id === noteId;
    });
    if(!note){
        location.assign('/index.html');
    }
    
    titleEl.value=note.title;
    dateEl.textContent=generateLastEdited(note.updatedAt);
    bodyEl.value=note.body;
};

const generateLastEdited = (timestamp) => {
    return `last edited ${moment(timestamp).fromNow()}`;
};

export {generateNoteDOM, renderNotes, generateLastEdited, initializeEditPage};