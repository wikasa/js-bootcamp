class Hangman {
    constructor(word = [], remainingGuesses){
        this.word = word.toLowerCase().split('');
        this.remainingGuesses = remainingGuesses;
        this.guessedLetters = [];
        this.status = 'playing';
    }
    setStatus(){
        const finished = this.word.every(letter => {
            return this.guessedLetters.includes(letter) || letter===' ';
        })
        if(this.remainingGuesses === 0){
            this.status = 'failed';
        }
        else if(finished){
            this.status = 'finished';
        }
        else{
            this.status = 'playing';
        }
    }
     get puzzle(){
        let puzzle = '';
        this.word.forEach((letter) => {
            this.guessedLetters.includes(letter) || letter === ' ' ? puzzle+=letter : puzzle+='*';
        })
        return puzzle;
    }
    makeGuess(guess){
        if(this.status !== 'playing'){
            return;
        }
        if(typeof(guess)==='string'){
            guess=guess.toLowerCase();
            const isUnique = !this.guessedLetters.includes(guess);
            const isBadGuess = !this.word.includes(guess);
            if(isUnique){
                this.guessedLetters=[...this.guessedLetters, guess];
            }
            if(isUnique && isBadGuess){
                this.remainingGuesses--;
            }
            this.setStatus();
        }
    }
     get statusMessage(){
        if(this.status === 'playing'){
            return `Guesses left: ${this.remainingGuesses}`;
        }
        else if(this.status === 'failed'){
            return `Nice try! The word was "${this.word.join('')}"`;
        }
        else{
            return 'Great work! You guessed the word!'
        }
    }
}
export {Hangman as default};