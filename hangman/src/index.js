import Hangman from './hangman';
import getPuzzle from './requests';

let hangman1;

const puzzleEl = document.querySelector('#puzzle');
const messageEl = document.querySelector('#status-message');

window.addEventListener('keypress', e => {
    if(!e.code.includes('Key')){
        return;
    }      
    const guess = e.code.slice(-1).toLowerCase();
    hangman1.makeGuess(guess);
    render();
});

const render = () => {
    puzzleEl.innerHTML='';
    messageEl.textContent = hangman1.statusMessage;
    hangman1.puzzle.split('').forEach(letter => {
        const letterEl = document.createElement('span');
        letterEl.textContent = letter;
        puzzleEl.appendChild(letterEl);
    });
}

const startGame = async () => {
    try{
        const puzzle = await getPuzzle('5');
        hangman1 = new Hangman(puzzle, 5);
        render();
    }
    catch(e){
        throw new Error('An error');
    }
}

document.querySelector('#reset').addEventListener('click', startGame);
startGame();