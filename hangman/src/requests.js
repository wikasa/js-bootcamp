const getPuzzle = async (wordCount) => {
    const response = await fetch(`//puzzle.mead.io/puzzle?wordCount=${wordCount}`);
    if(response.status===200){
        const data = await response.json();
        return data.puzzle;
    }
    else{
        throw new error('Unable to get puzzle');
    }
};

const getCountryDetails = async (countryCode) => {
    const response = await fetch('http://restcountries.eu/rest/v2/all');
    if(response.status===200){
        const countries = await response.json();
        const country = countries.find(country => country.alpha2Code === countryCode);
        return country.name;
    }
    else{
        throw new Error('Failed to fetch country code');
    }
};

const getLocation = async () => {
    const response = await fetch('https://ipinfo.io/json?token=7414623c6e676c');
    if(response.status === 200){
        return response.json();
    }
    else{
        throw new Error('An error');
    }
};

const getCurrentCountry = async () => {
    const location = await getLocation();
    return getCountryDetails(location.country);
}

export {getPuzzle as default};

// => new Promise((resolve, reject) => {
//     const countriesRequest = new XMLHttpRequest();
//     countriesRequest.addEventListener('readystatechange', e => {
//         if(e.target.readyState===4 && e.target.status===200){
//             const countriesInfo = JSON.parse(countriesRequest.responseText);
//             const isMyCountry = countriesInfo.find(country => country.alpha2Code === countryCode);
//             resolve(isMyCountry.name);
//         }
//         else if(e.target.readyState===4){
//             reject(new Error('An error'));
//         }
//     });
//     countriesRequest.open('GET', 'http://restcountries.eu/rest/v2/all');
//     countriesRequest.send();
// });


// const getPuzzleOld = (wordCount) => {
//     return fetch(`http://puzzle.mead.io/puzzle?wordCount=${wordCount}`).then((response) => {
//         if(response.status===200){
//             return response.json();
//         }
//         else{
//             throw new Error('An error occured');
//         }
//     }).then((data) => {
//         return data.puzzle;
//     });
// };