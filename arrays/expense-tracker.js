const account={
    name: 'Wiki',
    expenses:[],
    income:[],
    addExpense:function(description, amount){
        this.expenses.push({
            description: description, 
            amount:amount
        })
    },
    getAccountSummary:function(){
        let summaryExpenses=0
        let summaryIncome=0

        this.expenses.forEach(function(expense){
            summaryExpenses+=expense.amount
        })
        this.income.forEach(function(income){
            summaryIncome+=income.amount
        })
        
        const balance=summaryIncome-summaryExpenses
        return `${this.name} has balance of $${balance}. $${summaryIncome} in income. $${summaryExpenses} in expenses.`
    },
    addIncome: function(description, amount){
        this.income.push({
            description: description,
            amount: amount
        })
    }
}

account.addExpense('Pens', 10)
account.addExpense('Pencils', 25)
account.addIncome('Pocket money', 100)
console.log(account.getAccountSummary())