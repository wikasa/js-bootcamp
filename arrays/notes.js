const notes= [{
    title:'aNote 1 kwi',
    body: 'Spain'
}, {
    title: 'xNote 2',
    body:'Netflix'
}, {
    title: 'jNote 3',
    body: 'Excercise kwi'
}]

const sortNotes=function(notes){
    notes.sort(function(a, b){
        if(a.title.toLowerCase()<b.title.toLowerCase()){
            return -1
        } else if(b.title.toLowerCase()<a.title.toLowerCase()){
            return 1
        } else{
            return 0
        }
    })
}
const findNote=function(note, noteTitle){
    return note.find(function(item){
        return item.title.toUpperCase()===noteTitle.toUpperCase()
    })
}

const findNotes=function(notes, query){
    return notes.filter(function(note, index){
        const isTitleMatch=note.title.toLowerCase().includes(query.toLowerCase())
        const isBodyMatch=note.body.toLowerCase().includes(query)
        return isTitleMatch || isBodyMatch
    })
}

// console.log(findNotes(notes, 'ne'))

// const note=findNote(notes, 'note 2')
// console.log(note)

sortNotes(notes)
console.log(notes)




