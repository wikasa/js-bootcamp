const todo=[{text:'Water plants', completed: false}, {text: 'Write essay', completed:true}, {text: 'Read book', completed:false}, {text: 'Buy some lemons', completed:false},{text:'Make lemonade', completed:true}]

deleteTodo=function(todo, textSearched){
    const index=todo.findIndex(function(todo){
        return todo.text.toLowerCase()===textSearched.toLowerCase()
    })
    if(index > -1){
    todo.splice(index, 1)
    }
}

const isNotCompleted=function(todo){
    return todo.filter(function(todo){
        return !todo.completed
    })
}

const sortedTodo=function(todo){
    todo.sort(function(a, b){
        if(!a.completed && b.completed){
            return -1
        }else if(!b.completed && a.completed){
            return 1
        }else{
            return 0
        }
    })
}

sortedTodo(todo)
console.log(todo)
// console.log(isNotCompleted(todo))
// deleteTodo(todo, 'write essay')
// console.log(todo)