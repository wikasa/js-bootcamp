let restaurant= {
    name: 'Romeo',
    guestCapacity: 75,
    guestCount: 0,
    checkAvailability: function(partySize){
        let seatsLeft=this.guestCapacity-this.guestCount
        return partySize<=seatsLeft
    },
    seatParty: function(seated){
        this.guestCount+=seated
    },
    removeParty:function(seated){
        this.guestCount=this.guestCount-seated
    }
}

restaurant.seatParty(72)
console.log(restaurant.checkAvailability(4))
restaurant.removeParty(5)
console.log(restaurant.checkAvailability(4))