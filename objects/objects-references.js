let myAccount = {
    name: 'Wiki',
    expenses: 0,
    income: 0
}

let addExpense= function(account, total){
account.expenses=account.expenses+total
}

let addIncome=function(account, income){
account.income=account.income+income
}

let resetAccount=function(account){
    account.expenses=0
    account.income=0
}

let getAccountSummary=function(account){
    console.log(`${account.name} has $${account.income-account.expenses}. $${account.income} in income. $${account.expenses} in expenses.`)
}

addIncome(myAccount, 1000)
addExpense(myAccount, 200)
addExpense(myAccount, 300)
getAccountSummary(myAccount)
resetAccount(myAccount)
getAccountSummary(myAccount)