let myBook={
    title: '1984',
    author: 'George Orwell',
    pageCount: 326
}

let otherBook={
    title: 'Peoples history',
    author: 'Howard Zinn',
    pageCount: 723
}

let getSummary=function(book){
    return{
        summary:`${book.title} by ${book.author}`,
        pageCountSummary:`${book.title} is ${book.pageCount} pages long`
    }
}

let bookSummary=getSummary(myBook)
let otherBookSummary=getSummary(otherBook)

console.log(bookSummary.pageCountSummary)
console.log(otherBookSummary)


let converter=function(temperature){
    return{
        Fahrenheit: temperature,
        Celsius: (temperature-32)*(5/9),
        Kelwin:(temperature+459.67)*(5/9)
    }
}

let conversion=converter(74)
console.log(`${conversion.Fahrenheit}, ${conversion.Celsius}, ${conversion.Kelwin}`)
console.log(conversion)

